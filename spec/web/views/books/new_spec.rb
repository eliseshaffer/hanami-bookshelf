require_relative '../../../spec_helper'

describe Web::Views::Books::New do
  let(:params)    { OpenStruct.new(valid?: false, error_messages: ['Title must be filled in', 'Author must be filled in']) }
  let(:exposures) { Hash[params: params] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/books/new.html.erb') }
  let(:view)      { Web::Views::Books::New.new(template, exposures) }
  let(:rendered)  { view.render }

  it 'renders errors when parmas contain errors' do
    rendered.must_include 'There was a problem with your submission'
    rendered.must_include 'Title must be filled in'
    rendered.must_include 'Author must be filled in'
  end
end
