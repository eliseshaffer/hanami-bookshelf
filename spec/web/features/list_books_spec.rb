require 'features_helper'

describe 'List Books' do
  let(:author_repository)  { AuthorRepository.new }
  let(:book_repository) { BookRepository.new }

  before do
    book_repository.clear
    author_repository.clear

    @kent_beck = author_repository.create(name: 'Kent Beck')
    @sandi_metz = author_repository.create(name: 'Sandi Metz')

    book_repository.create(title: 'Refactoring', author_id: @kent_beck.id)
    book_repository.create(title: 'POODR', author_id: @sandi_metz.id)
  end

  it 'displays each book on a page' do
    visit '/books'

    within '#books' do
      assert page.has_css?('.book', count: 2), 'Expected to find 2 books'
      assert page.has_content?('Kent Beck')
      assert page.has_content?('Sandi Metz')
    end
  end

  it 'can navigate to author list page' do
    visit '/books'

    within '#books' do
      click_on 'Kent Beck'
    end

    current_path.must_equal "/authors/#{@kent_beck.id}"
  end
end
