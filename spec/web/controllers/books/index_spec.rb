require_relative '../../../spec_helper'

describe Web::Controllers::Books::Index do
  let(:action) { Web::Controllers::Books::Index.new }
  let(:params) { Hash[] }
  let(:book_repository) { BookRepository.new }
  let(:author_repository) { AuthorRepository.new }

  before do
    book_repository.clear
    author_repository.clear

    @author = author_repository.create(name: 'Kent Beck')
    @book = book_repository.create(title: 'TDD', author_id: @author.id)
  end

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end

  it 'exposes all books with authors' do
    action.call(params)
    action.exposures[:books].must_equal [@book]
    action.exposures[:books].first.author.must_equal @author
  end
end
