require_relative '../../spec_helper'

describe BookRepository do
  let(:book_repository) { BookRepository.new }
  let(:author_repository) { AuthorRepository.new }

  describe '#all_with_books' do
    before do
      author_repository.clear
      book_repository.clear

      @sandi = author_repository.create(name: 'Sandi Metz')
      @poodr = book_repository.create(title: 'POODR', author_id: @sandi.id)
      @bottles = book_repository.create(title: '99 Bottle of OOP', author_id: @sandi.id)
    end

    it 'returns books with related authors' do
      books = book_repository.all_with_authors

      books.must_equal [@poodr, @bottles]
      books.each do |book|
        book.author.must_equal @sandi
      end
    end
  end

  describe '#create_with_author' do
    it 'creates a book with a a new author' do
      book = book_repository.create_with_author(title: 'Refactoring', author: 'Kent Beck')

      book.title.must_equal 'Refactoring'
      book.author.must_be_instance_of Author
      book.author.name.must_equal 'Kent Beck'
    end
  end
end
