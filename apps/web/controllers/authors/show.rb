module Web::Controllers::Authors
  class Show
    include Web::Action

    expose :author

    def call(params)
      @author = AuthorRepository.new.find_with_books(params.dig(:id))
    end
  end
end
